angular.module("myApp").controller("MeCtrl",function($scope,UserService){
    UserService.getMe()
    .then(function success(res){
        $scope.http={
            statusText:res.statusText,
            status:res.status
        }
        $scope.user=res.data.data
        console.log("Dati recuperati!!!","Dati:",res)
       
    }, function fail(res){
         $scope.http={
            statusText:res.statusText,
            status:res.status
        }
        $scope.message=res.data.message
        console.log("Errore!!!","Risposta:",res)
    })
    
})