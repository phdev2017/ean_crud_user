angular.module("myApp").controller("HomeCtrl",function($scope,UserService){//dentro la funzione vanno inseriti i nomi degli oggetti che usi (dependecy injection)
    /*
    Lo $scope in angular è un oggetto particolare che impiega angular per "mettere in comunicazione" una view con il controller.
    Permette al controller di aggiungere e modificare le proprietà a cui potrebbe accedere la view (html)
    Inolre permette di implementare questo aspetto in metodi diversi:
     ->two way binding: se modifichi dalla view o dal controller il dato si aggiorna in automatico (viene impiegato con oggetti)
     ->one way b. : permette di passare una stringa; essa non verrà aggiornata dall'altra parte nel caso in cui venga modificata
    */
    $scope.logout=function(){//sto dicendo allo scope di aggiungere la proprietà logout (funzione)
        console.log("Logout");
        delete window.localStorage.token;//cancella il token dal localstorage
    }
})