angular.module("myApp").controller("LoginCtrl",function($scope,UserService,$state){
  $scope.page=$state.$current.name//state è un oggetto che permette di sapere lo stato attuale (associazione view+controller+url) o di modificarlo (ci si sposta di pagina in pagina)
  $scope.submit=function(){
      UserService.login($scope.user).then(function success(res){//Userservice.login sta ricevendo l'user dallo scope e restituisce una promessa.
          //Quando fai una chiamata http otterrai una promessa ($q), ovvero qualcosa che arriverà nel futuro! E' "un'alternativa" alle callback.
          //Per gestirle basta usare il metodo .then(successFunction(res){},errorFunction(res){})
        window.localStorage.token=res.data.data;//salvati il token in localstorage
        $state.go("me")//cambia stato (pagina)
        console.log("Ti sei loggato!!!","Dati:",res)

    }, function fail(res){//non è necessario mettere il nome, si può lasciare anche una funzione anonima (appunto senza nome!) ma in caso di errori sarà più facile capire che funzione scatena l'eccezione
        console.log("Errore!!!","Risposta:",res)
        $state.go("signup")
    })
  }  
})