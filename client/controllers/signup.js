angular.module("myApp").controller("SignUpCtrl",function($scope,$state,UserService){
  $scope.page=$state.$current.name
  $scope.submit=function(){
    UserService.signup($scope.user).then(function success(res){
        window.localStorage.token=res.data.data;
        $state.go("me")
        console.log("Ti sei loggato!!!","Dati:",res)

    }, function fail(res){
        console.log("Errore!!!","Risposta:",res)
    })
  }  
})