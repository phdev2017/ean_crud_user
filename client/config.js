//sto recuperando l'app angular e gli sto aggiungendo un metodo config. Il config è un metodo particolare che viene richiamato prima di eseguire l'app! Dentro questo metodo si configura la struttura dell'applicazione e gli interceptors
angular.module("myApp").config(function Configuration($stateProvider, $urlRouterProvider,$httpProvider){ 
    
    
    
    
    $urlRouterProvider.otherwise('/home');//se scrivi un url che non è mappato richiama home
    
     $stateProvider
        .state('home', {//stato --> come se fosse un'associazione logica
          url: '/home',//url 
          controller: 'HomeCtrl',//file javascript che lavora con la view specificata
          templateUrl: 'views/home.html'//view
        })
        .state('login', {
          url: '/login',
          controller: 'LoginCtrl',
          templateUrl: 'views/form.html'//*
        })
        .state('me', {
          url: '/me',
          controller: 'MeCtrl',
          templateUrl: 'views/me.html'
        })
         .state('signup', {
          url: '/signup',
          controller: 'SignUpCtrl',
          templateUrl: 'views/form.html'//*
        })
     
     //* posso applicare a due stati lo stesso controller o view
     
     //Viene richiamato ad ogni chiamata http!
     $httpProvider.interceptors.push(function() {

             return {
                 'request': function (config) {
                    var token=window.localStorage.token;//accedo al localstorage cercando la propietà token
                    console.log(token);
                    if (token!==undefined) {
                        config.headers.Authorization = 'Bearer ' + token;//setto sull'oggetto config la propietà headers in modo che abbia il campo Authorization settato con il valore del mio token
                    }
                    return config;//restituisco l'oggetto config usato per eseguire la chiamata http 
                 },
//                 'responseError': function(response) {
//                 
//                 }
             };
         });
})



//N.B. dentro al config si usano INTERAMENTE i providers (da approfondire più avanti)