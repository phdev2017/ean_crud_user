angular.module("myApp").service("UserService",function($http){
    return{
        getMe:function(){
            return $http.get("/api/me");
        },
        login:function(user){
            return $http.post("/api/login",user);
        },
        signup:function(user){
            return $http.post("/api/signup",user);
        }
    }
})