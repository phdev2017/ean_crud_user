/*
|--------------------------------------------------------------------------
| Login Required Middleware
|--------------------------------------------------------------------------
*/
var jwt = require('jwt-simple');
var moment = require("moment");
var config = require('./config')
var authLevel={}
var collection=[];

authLevel.setCollection=function(c){
    collection=c;
};

authLevel.findById=function(idUser){
    var id = idUser;

    for(var i=0;i<collection.length;i++){  
        console.log(collection[i]);
        if (collection[i].id==id) {
            return collection[i];
        }
    }
}

authLevel.findByEmailAndPassword=function(user){
    var email = user.email;
    var passwd = user.passwd;

    
    for(var i=0;i<collection.length;i++){  
        console.log(collection[i]);
        if (collection[i].email==email && collection[i].passwd==passwd) {
            return collection[i];
        }
    }
}

authLevel.lookingForDuplicates=function(user){
    var email = user.email;

    for(var i=0;i<collection.length;i++){  
        console.log(collection[i]);
        if (collection[i].email==email) {
            return true;
        }
    }
    return false
}

authLevel.createJWT=function(user) {
  var payload = {
    sub: user.id,//id user
    iat: moment().unix(),//tempo di creazione
    exp: moment().add(14, 'days').unix()//scadenza del token tra 14 g da ora
  };
  return jwt.encode(payload, config.TOKEN_SECRET);//crea il token a partire dall'oggetto e da una chiave segreta (stringa)
}
    
authLevel.ensureAuthenticated=function(req, res, next) {
  if (!req.header('Authorization')) {
    return res.status(401).send({ message: 'Please make sure your request has an Authorization header' });
  }
  var token = req.header('Authorization').split(' ')[1];

  var payload = null;
  try {
    payload = jwt.decode(token, config.TOKEN_SECRET);
  }
  catch (err) {
    return res.status(401).send({ message: err.message });
  }

  if (payload.exp <= moment().unix()) {
    return res.status(401).send({ message: 'Token has expired' });
  }

    //dovrebbe esserci un db
    user=authLevel.findById(payload.sub);
    if (!user) {
      payload.sub=undefined;
      return res.status(401).send({ message: 'User does not exists' });
    }if(payload.sub!==undefined){
      req.user = payload.sub;
      next();
    }else{
      return res.status(401).end();
    }
}


authLevel.deleteToken=function(req,res,next){
    if (!req.header('Authorization')) {
        return res.status(401).send({ message: 'Please make sure your request has an Authorization header' });
    }else{
        var token=req.header('Authorization')
        console.log("token prima:",token)
        req.headers.authorization=undefined;
        var token=req.header('Authorization')
        console.log("token ora:",token)
        next();
    }
}

module.exports = authLevel;