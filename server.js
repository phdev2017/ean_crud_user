var bodyParser = require('body-parser'); //per il parsing degli oggetti contenuti nel body (capirete)
var express = require('express');//serve a gestire le nostre api

var app = express(); //costruttore di express
var port_number = process.env.PORT || 3000;//porta del nostro server
var moment = require("moment");
var jwt = require('jwt-simple');
var authLevel = require('./authLevel');
var path = require('path');


//Parsing body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));



app.use('/', express.static(path.join(__dirname, 'client')));//applicazione angular




//"DB", da sostituire
var id=3;
var dummyUser=[{id:1,email:"ludovico.sidari@icloud.com",passwd:"pippo"},{id:2,email:"luca.rossi@icloud.com",passwd:"pippo"}];

authLevel.setCollection(dummyUser);





/*
* GET API
*/
app.get("/api/me",authLevel.ensureAuthenticated,function (req, res) { //expressOBJ.httpMethod("urlPerRichiamare",funzioneDiCallback)
    var id = req.user; 

    var userMe=authLevel.findById(id);
    if(!userMe){
         return res.status(500).send({err:"user non esistente",data:undefined});//se non lo dovessi trovare dovrei rispondere http->500, l'user selezionato non esiste
    }
    return res.status(200).send({err:undefined,data:userMe});//se lo dovessi trovare rispondo con http->200 e passo
});



app.post("/api/login",function(req,res){
    var user=req.body;//accedo al body
    if(user.email===undefined){//controllo dei campi passati
        return res.status(500).send({err:"manca l'email nell'oggetto user che hai passato",data:undefined});
    }else if(user.passwd===undefined){//controllo dei campi passati
        return res.status(500).send({err:"manca la password nell'oggetto user che hai passato",data:undefined});
    }else{
        var userCheck=authLevel.findByEmailAndPassword(user);
        if(userCheck){
            var token=authLevel.createJWT(userCheck);
            return res.status(200).send({err:undefined,data:token});//se lo dovessi trovare rispondo con http->200 e passo l'user
        }
        return res.status(500).send({err:"nessun user trovato",data:undefined});
    }
})

//app.get("/api/logout",authLevel.ensureAuthenticated,authLevel.deleteToken,function(req,res){
//    return res.status(200).send();
//})


app.post("/api/signup",function(req,res){
    var user = req.body;//accedo al body per salvarmi i dati passati dalla post
    user.id=id++;//non si fa così, ma per ora ce lo facciamo andare bene
    
    var duplicated=authLevel.lookingForDuplicates(user);
    if(duplicated){
        return res.status(500).send({err:"user esiste già",data:undefined});
    }
    
    dummyUser.push(user);//aggiungo l'user

    //cerco di recuperare l'user dal mio array
    console.log(dummyUser);
    
    var userToMakeToken=authLevel.findById(user.id);
    if(!userToMakeToken){
         return res.status(500).send({err:"user non esistente",data:undefined});//se non lo dovessi trovare dovrei rispondere http->500, l'user selezionato non esiste
    }
    var token=authLevel.createJWT(userToMakeToken);
    return res.status(200).send({err:undefined,data:token});//se lo dovessi trovare rispondo con http->200 e passo
   
})


app.listen(port_number, function() { //rimani in ascolto sulla porta specificata per il server http
  console.log('Express server listening on port ' + port_number);
});
